import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

const WhyCard = ({ CardTitle, CardIcon, IconColor, CardDesc }) => {
  return (
    <div className="whycard cursor-pointer">
      <div className="whycard-bg">
        <div className="whycard-bghover"></div>
      </div>
      <div className="whycard-content">
        <div className="w-14 h-12 flex items-end justify-start relative">
          <FontAwesomeIcon
            icon={CardIcon}
            className={`${IconColor} w-16 h-16 left-0 absolute -mt-14 top-1/2`}
          />
        </div>
        <h3 className="text-4xl font-normal mt-5 text-gray-800">{CardTitle}</h3>
        <p className="!text-xl !mt-5 text-gray-500">{CardDesc}</p>
      </div>
    </div>
  );
};

export default WhyCard;
