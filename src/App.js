import {
  faAward,
  faBullseye,
  faChartPie,
  faFilterCircleDollar,
  faUsersGear,
} from "@fortawesome/free-solid-svg-icons";
import WhyCard from "./WhyCard";

function App() {
  return (
    <div className="py-28 bg-slate-600">
      <div className="bg-white">
        <div className="flex flex-wrap justify-end">
          <div className="my-auto w-full max-w-4xl py-24 px-20 space-y-4">
            <h2 className="text-5xl font-bold text-indigo-900 leading-snug">
              Why The Industry <br /> Chooses Clickdee?
            </h2>
            <p className="text-xl text-gray-600 font-light">
              We understand performance marketing from every angle and every
              stage of the funnel. Our clients trust that we know what metrics
              move their business towards growth. Our publisher and affiliate
              partners know that we make maximum revenue and ROAS a main focus
              when growing our partnerships.
            </p>
          </div>

          <WhyCard
            CardTitle={"Choose Your Local Targeting"}
            CardIcon={faBullseye}
            IconColor={"text-rose-400"}
            CardDesc={
              "Our targeted and tracked calls are tailored to your business needs, audience, and geolocation."
            }
          />

          <WhyCard
            CardTitle={"Track Your Conversion"}
            CardIcon={faFilterCircleDollar}
            IconColor={"text-blue-400"}
            CardDesc={
              "We use the most progressive tracking and analytics technology to ensure that every call can be tracked and evaluated."
            }
          />

          <WhyCard
            CardTitle={"Customized Campaigns"}
            CardIcon={faChartPie}
            IconColor={"text-purple-400"}
            CardDesc={
              "Audiences, budgets, and goals: you decide the criteria for your digital ad campaign, and we’ll do the rest."
            }
          />

          <WhyCard
            CardTitle={"Get Dedicated Support Team"}
            CardIcon={faUsersGear}
            IconColor={"text-orange-400"}
            CardDesc={
              "We constantly monitor quality control! Our affiliate network of publishers are digital ad experts that have been vetted."
            }
          />

          <WhyCard
            CardTitle={" Quality Assurance"}
            CardIcon={faAward}
            IconColor={"text-red-400"}
            CardDesc={
              "We constantly monitor quality control! Our affiliate network of publishers are digital ad experts that have been vetted."
            }
          />
        </div>
      </div>
    </div>
  );
}

export default App;
